<?php

/**
 * @file
 * Administrative form for setting the live_maintenance_mode module.
 */

/**
 * Form builder for Live Maintenance Mode settings.
 *
 * @see system_settings_form()
 */
function live_maintenance_mode_settings_form($form, &$form_state) {
  $form['live_maintenance_mode_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable live maintenance mode'),
    '#default_value' => variable_get('live_maintenance_mode_enabled', ''),
    '#description' => t('When enabled, all site users will be blocked from logging in while your site is live and without having to put your site into maintenance mode.'),
  );

  $form['live_maintenance_mode_message'] = array(
    '#type' => 'textarea',
    '#title' => t('Maintenance Message'),
    '#required' => TRUE,
    '#default_value' => variable_get('live_maintenance_mode_message', t('Website is currently under maintenance. User accounts have been blocked temporarily.')),
    '#description' => t('Message to users when Live Maintenance Mode is enabled. This will be displayed on the login page.'),
  );

  $form['live_maintenance_mode_usernames'] = array(
    '#type' => 'textfield',
    '#title' => t('Additional Usernames'),
    '#default_value' => variable_get('live_maintenance_mode_usernames', ''),
    '#description' => t('By default, user with uid 1 is allowed to login. Enter additional usernames in the field above to allow login. Separate usernames with a comma.'),
  );

  return system_settings_form($form);
}
