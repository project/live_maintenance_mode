Live Maintenance Mode
---------------------

INTRODUCTION
------------

Block all site users from logging in while your site is live and 
without having to put your site into maintenance mode. 

This functionality is useful for busy sites when administrators 
are applying maintenance updates or other configuration changes that 
require a maintenance window, but do not necessarily want to put the 
site into full maintenance mode.

When active, the module displays a warning message on the login screen 
alerting users that maintenance is underway and that they are 
temporarily blocked. User with uid=1 is allowed to login by default, 
but additional usernames can be allowed. Current logged-in users will 
automatically be logged out.

REQUIREMENTS
------------

None

INSTALLATION
------------
1. Download and install Live Maintenance Mode module.
2. Visit the Modules page of your site and enable the module. Note that
   once enabled, the site will be in Live Maintenance Mode.
3. Go to Configuration > Development > Live Maintenance Mode to configure
   default messaging and allowed users.

CONFIGURATION
-------------
1. Enable the Live Maintenance Mode module from the Modules page.
	 - Note that once enabled, the site will be in Live Maintenance Mode, meaning
	 all users except user with uid 1 (and any other users set in step 2) will be
	 blocked and logged out.
2. Go to Configuration > Development > Live Maintenance Mode to configure
   default messaging and allowed users.
